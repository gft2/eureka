## Eureka Service Server


### Instructions

Go to [account gft project](https://gitlab.com/gft2/account)

1.- clone account repo
 ```sh 
 git clone https://gitlab.com/gft2/account.git
``` 

2.- Compile
 ```sh 
 mvn clean compile install
 ```

3.-  Start a instance
 ```sh
 mvn clean spring-boot:run
```
4.- Again start a second new instance
 ```sh 
 mvn clean spring-boot:run
 ```

Go to [client gft project](https://gitlab.com/gft2/client)

1.- clone account repo
 ```sh 
 git clone https://gitlab.com/gft2/client.git
```

2.- Compile
 ```sh
 mvn clean compile install
 ```

3.-  start an instance
 ```sh
 mvn clean spring-boot:run
```

...finally  clone eureka repo

```sh
git clone git@gitlab.com:gft2/eureka.git
```

Open your browser in eureka service
* http://localhost:8761


![](https://i.imgur.com/r9C9yyB.png)

Open client serice and test with


***/gft-bank/client/{id}/accounts/*** (id=1)

![](https://i.imgur.com/ODD7yCa.png)